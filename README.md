# cypress-kraken

A tool to run cypress tests in parallel

## Features

- Running cypress tests in parallel
- Specify a maximum amount of tests to run per worker
- Bail if one test has failed

## Installation

```bash
$  yarn add -D cypress-kraken
```

or:

```bash
$  npm i -D cypress-kraken
```

## Usage

Example on how to run the tool with minimal options:

```bash
$  yarn cypress-kraken -c "yarn cy:run"
```

For CI env it may be necessary to run `Xvfb` before cypress:

```bash
$  Xvfb :99 &
$  export DISPLAY=:99
$  yarn cypress-kraken -c "yarn cy:run"
$  pkill Xvfb
```

Ref: https://docs.cypress.io/guides/guides/continuous-integration.html#Xvfb

### CLI Options

#### -c, --cmd <cmd>

Cypress command to execute. It could be anything you want, and it could even have args.
Defaults to `yarn cypress run`.

Samples: `yarn cy:run` / `npm run cypress:run` / `yarn cy:run --config baseUrl=http://localhost:3000`

#### -w, --workers <num>

Specifies the number of workers the worker-pool will spawn for running tests.
Defaults to the number of the cores available on your machine divide by 2.

#### --integrationFolder <path>

Path to folder containing integration test files.
Defaults to `cypress/integration`.

#### --retries <num>

Number of times that failed tests should be retry, before considere them failed.
Defaults to `2`.

#### --testFiles <globs>

A String or Array of glob patterns of the test files to load.
Defaults to `**/*.*`.

#### --testPathPattern <regex>

A regexp pattern string that is matched against all tests paths before executing the test. On Windows, you will need to use `/` as a path separator or escape as `\\`.

#### --maxConcurrency <num>

Prevents Jest from executing more than the specified amount of tests at the same time.
Defaults to `5`.

#### -b, --bail

Exit the test suite immediately upon 1 failing test suite
Defaults to `false`.

## Troubleshooting

- Xvfb exited with a non zero exit code. There was a problem spawning Xvfb.

```
Error: (EE)
Fatal server error:
(EE) Server is already active for display 99
If this server is no longer running, remove /tmp/.X99-lock
and start again.
(EE)
```

**Solution:**

When running on Linux, Cypress needs an X11 server; otherwise it spawns its own X11 server during the test run. When running several Cypress instances in parallel, the spawning of multiple X11 servers at once can cause problems for some of them. In this case, you can separately start a single X11 server and pass the server’s address to each Cypress instance using DISPLAY variable.

First, spawn the X11 server in the background at some port, for example :99. If you have installed xvfb on Linux or if you are using one of our Docker images from cypress-docker-images, the tools below should be available.

```bash
$  Xvfb :99 &
```

Second, set the X11 address in an environment variable

```bash
$  export DISPLAY=:99
```

Start Cypress as usual

```bash
$  yarn cypress-kraken -c "yarn cy:run"
```

After all tests across all Cypress instances finish, kill the Xvfb background process using pkill

```bash
$  pkill Xvfb
```

In certain Linux environments, you may experience connection errors with your X11 server. In this case, you may need to start Xvfb with the following command:

```bash
$  Xvfb -screen 0 1024x768x24 :99 &
```

Cypress internally passes these Xvfb arguments, but if you are spawning your own Xvfb, you would need to pass these arguments. This is necessary to avoid using 8-bit color depth with Xvfb, which will prevent Chrome or Electron from crashing.

```bash
$  xvfb-run -a yarn cypress:run
```

Ref: https://docs.cypress.io/guides/guides/continuous-integration.html#Xvfb

## Releasing/Publishing

```bash
$  yarn version --minor
```

```bash
$  git push --follow-tags
```

```bash
$  npm publish
```

## Todos

- try using the module api
- analize results at least from last ran, and sort them by time, and exec the ones that take the most in parallel with those that dont. a lot could even be empty, so all of those could be run together at once. basically pick smartly the specs to run

// `yarn ${script} --reporter-options reportDir=cypress/results/${currentTime} -s ${tests.join(',')}`,

// "mocha": "^7.2.0",
// "mochawesome": "^6.1.1",

// "reporter": "mochawesome",
// "reporterOptions": {
// "reportDir": "cypress/results",
// "overwrite": false,
// "html": false,
// "json": true,
// "toConsole": false
// },
