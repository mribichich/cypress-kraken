#!/usr/bin/env node

const glob = require('glob')
const { exec } = require('child_process')
const { program } = require('commander')
const { version } = require('../package.json')
const path = require('path')
const os = require('os')
const {
  parseRegex,
  exitOnError,
  findFailedTestsInStdout,
  removeIntegrationFolder,
  appendIntegrationFolder,
} = require('./utils')

program
  .version(version)
  .option('-b, --bail', 'Exit the test suite immediately upon 1 failing test suite', false)
  .option('-c, --cmd <cmd>', 'Cypress command to execute', 'yarn cypress run')
  .option('--integrationFolder <path>', 'Path to folder containing integration test files', 'cypress/integration')
  .option(
    '--maxConcurrency <num>',
    'Prevents Jest from executing more than the specified amount of tests at the same time',
    5
  )
  .option('--retries <num>', 'Number of times that failed tests should be retry, before considere them failed', 2)
  .option('--testFiles <globs...>', 'A String or Array of glob patterns of the test files to load', ['**/*.*'])
  .option(
    '--testPathPattern <regex>',
    'A regexp pattern string that is matched against all tests paths before executing the test. On Windows, you will need to use / as a path separator or escape  as \\.',
    parseRegex
  )
  .option(
    '-w, --workers <num>',
    'Specifies the number of workers the worker-pool will spawn for running tests. This defaults to the number of the cores available on your machine divide by 2',
    Math.floor(os.cpus().length / 2)
  )

program.parse(process.argv)

const { bail, cmd, integrationFolder, maxConcurrency, retries, testFiles, testPathPattern, workers } = program

/** @type {string[]} */
const allSpecs = testFiles.map((m) => glob.sync(path.join(integrationFolder, m))).flat()

const specsToRun = testPathPattern ? allSpecs.filter((specPath) => testPathPattern.test(specPath)) : allSpecs

process.stdout.write(`Running cypress in parallel with ${workers} workers\n`)
process.stdout.write(`\nFound ${specsToRun.length} spec files\n\n`)

/** @type {string[]} */
let errors = []
const runningProcesses = new Map()
let remainingRetries = Number(retries)

/**
 * @param {string[]} tests
 */
const cypressTask = (tests) => {
  process.stdout.write(`\nRunning (${specsToRun.length} remaining):\n`)
  process.stdout.write(
    `${removeIntegrationFolder(integrationFolder, tests)
      .map((m) => `  - ${m}`)
      .join('\n')}\n`
  )

  return new Promise((resolve) => {
    const p = exec(`${cmd} -s ${tests.join(',')}`, (err, stdout, stderr) => {
      if (err) {
        process.stdout.write('\n')
        process.stdout.write(stdout)
        process.stdout.write(stderr)
        process.stdout.write('\n')

        const failedTestsWithPartialPath = findFailedTestsInStdout(stdout)

        if (remainingRetries > 0) {
          remainingRetries--
          specsToRun.push(...appendIntegrationFolder(integrationFolder, failedTestsWithPartialPath))

          process.stdout.write(`Adding to retry:\n${failedTestsWithPartialPath.map((m) => `  - ${m}`).join('\n')}\n`)
        } else {
          errors.push(...failedTestsWithPartialPath)

          bail && exitOnError(errors, runningProcesses)
        }
      }

      runningProcesses.delete(p)
      resolve()
    })

    runningProcesses.set(p, p)
  })
}

function next() {
  if (specsToRun.length > 0) {
    return cypressTask(specsToRun.splice(0, maxConcurrency))
  }

  return Promise.resolve('done')
}

const chainer = () => {
  return new Promise((resolve) =>
    (function recursive() {
      next().then((res) => (res === 'done' ? resolve() : recursive()))
    })()
  )
}

let chains = Array.from({ length: workers }, chainer)

;(async () => {
  await Promise.all(chains)

  if (errors.length === 0) {
    console.log('\x1b[32m%s\x1b[0m', '\n✔ Successful All Passed!\n')
    process.exit(0)
  } else {
    exitOnError(errors)
  }
})()
