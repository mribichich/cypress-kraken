require('string.prototype.matchall')
const findLast = require('ramda/src/findLast')
const map = require('ramda/src/map')
const path = require('path')

exports.parseRegex = function (value) {
  return value ? new RegExp(value) : undefined
}

/**
 * @param {string} integrationFolder
 * @param {string[]} tests
 */
exports.removeIntegrationFolder = function (integrationFolder, tests) {
  return map((m) => m.replace(`${integrationFolder}/`, ''), tests)
}

/**
 * @param {string} integrationFolder
 * @param {string[]} tests
 */
exports.appendIntegrationFolder = function (integrationFolder, tests) {
  return map((m) => path.join(integrationFolder, m), tests)
}

/**
 *
 * @param {string[]} errors
 * @param {ChildProcess[]} processes
 */
exports.exitOnError = function (errors, processes = []) {
  console.log('\x1b[31m%s\x1b[0m', '\n✗ Some Failed: \n')
  console.log(errors.map((m) => `    ✗  ${m}`).join('\n'))
  console.log('\n')

  processes.forEach((p) => p.kill())

  process.exit(1)
}

/**
 * @param {string} stdout
 */
exports.findFailedTestsInStdout = function (stdout) {
  const failingTests = [...stdout.matchAll(/Failing:\u001b\[39m\s*\u001b\[31m/gm)]
  const runningTests = [...stdout.matchAll(/Running:\s*\u001b\[90m([^\u001b]*)\u001b\[39m/gm)]

  return map((f) => findLast((r) => r.index < f.index, runningTests)[1].trim(), failingTests)
}
